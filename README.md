# pcp: Progressive Copy

A very simple alternative to `cp` with progress bars.

## Why not `pv`?

Well personally I'll probably continue using that because I don't trust my own
tool, but `pv` is annoying because while you'd normally run `pv src > dest`,
with root you'd have to run `pv src | sudo tee dest &>/dev/null` and that's
annoying.

## Is there no better alternative?

Well, `dd` could probably do what you want, just

``` bash
pcp() {
  : "${1?No input file}"
  : "${2?No input file}"
  du -h "$1"
  dd if="$1" of="$2" bs=1M status=progress
}
```

Well, it wouldn't have the same effect on directories because `pcp` also
handles that, but who would ever want progress bars when copying whole
directories?

## Why aren't you pushing your own product in this README you absolute weirdo

Because the code is hairy and I don't know how to fix it
