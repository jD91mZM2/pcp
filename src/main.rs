use async_std::{
    channel,
    fs::{self, File},
    os::unix::fs::symlink,
    prelude::*,
    task,
};
use anyhow::{anyhow, bail, Context, Result};
use argh::FromArgs;
use walkdir::WalkDir;

use std::{
    fs as std_fs, 
    path::{PathBuf, Path},
    str,
    sync::atomic::{AtomicU64, Ordering},
    time::Duration,
};

#[derive(FromArgs, Debug)]
/// Copy with progress bar
struct Cli {
    #[argh(positional)]
    /// one or more input filenames and one output filename
    files: Vec<PathBuf>,

    #[argh(switch, short = 'r')]
    /// whether or not allow
    recursive: bool,
}

#[async_std::main]
async fn main() -> Result<()> {
    let cli: Cli = argh::from_env();

    if cli.files.len() < 2 {
        bail!("Cannot copy fewer than 2 elements");
    }

    let app = Box::leak(Box::new(App {
        transfers: Vec::new(),
        cli,
    }));

    let files = app.cli.files.clone();
    let mut iter = files.iter().map(|p| &**p);
    let output = iter.next_back().expect("expected non-zero iterator");
    let inputs: Vec<_> = iter.collect();

    app.copy(&inputs, output)?;

    let mut tasks = Vec::with_capacity(app.transfers.len());

    let (tx, rx) = channel::bounded(4);

    for transfer in app.transfers.iter() {
        let tx = tx.clone();
        let rx = rx.clone();
        tasks.push(task::spawn(async move {
            // If this channel is filled, that means we shouldn't be creating more tasks. Wait.
            tx.send(()).await.expect("failed to send via channel");

            // Execute inner
            let res = transfer.start(1024 * 1024 * 4).await;

            // Leave room for one more execution
            rx.recv().await.expect("failed to receive from channel");

            res
        }));
    }

    let app = &*app;
    let main = task::spawn(async move {
        loop {
            // Find first 4 active transfers
            let transfers = app.transfers.iter()
                .map(|t| (&t.name, f64::from_bits(t.progress.load(Ordering::Relaxed))))
                .filter(|&(_, p)| p >= f64::EPSILON && (1.0 - p).abs() >= f64::EPSILON)
                .take(4);

            let lines_printed = print_status(transfers).await;

            task::sleep(Duration::from_millis(100)).await;

            if lines_printed > 0 {
                print!("{}", termion::cursor::Up(lines_printed as u16));
            }
        }
    });

    for task in tasks {
        task.await?;
    }

    main.cancel().await;

    Ok(())
}

async fn print_status<'a, I>(iter: I) -> usize
where
    I: Iterator<Item = (&'a String, f64)> + Clone
{
    let (width, _) = termion::terminal_size().unwrap_or((20, 0));

    let longest = iter.clone()
        .map(|(n, _)| n.chars().count())
        .max()
        .unwrap_or(0);

    let mut count = 0;
    for (name, progress) in iter {
        print!("{}", termion::clear::AfterCursor);

        let name_padding = str::repeat(" ", longest - name.chars().count());

        let total = width as usize - longest - 3;
        let done = (progress * total as f64) as usize;
        let remaining = total - done;

        println!("{}{} [{}>{}]", name, name_padding, str::repeat("=", done), str::repeat(" ", remaining - 1));

        count += 1;
    }
    count
}

#[derive(Debug)]
struct App {
    transfers: Vec<Transfer>,
    cli: Cli,
}

impl App {
    fn output_for(&self, input: &Path, output: &Path) -> Result<PathBuf> {
        let file_name = input.file_name().ok_or(anyhow!("input file had no name"))?;
        Ok(output.join(file_name))
    }
    fn register_transfer(&mut self, input: PathBuf, output: PathBuf) -> Result<()> {
        if input.symlink_metadata()?.file_type().is_dir() {
            if !self.cli.recursive {
                bail!("Cannot copy directories without -r");
            }

            for entry in WalkDir::new(&input) {
                let entry = entry?;
                let abspath = entry.path();
                let relpath = abspath.strip_prefix(&input).context("error while getting file's relative path")?;
                let output = output.join(relpath);
                if entry.file_type().is_dir() {
                    std_fs::create_dir(output).context("failed to create output directory")?;
                } else {
                    self.transfers.push(Transfer::new(entry.into_path(), output));
                }
            }
        } else {
            self.transfers.push(Transfer::new(input, output));
        }
        Ok(())
    }
    fn copy_one(&mut self, input: &Path, output: &Path) -> Result<()> {
        if output.is_dir() {
            let output = self.output_for(input, output)?;
            self.register_transfer(input.to_owned(), output)?;
        } else {
            self.register_transfer(input.to_owned(), output.to_owned())?;
        }
        Ok(())
    }
    fn copy_multi(&mut self, inputs: &[&Path], output: &Path) -> Result<()> {
        let exists = output.exists();
        if exists && !output.is_dir() {
            bail!("Multiple files were given, but output is not a directory");
        }

        if !exists {
            std_fs::create_dir(output).context("failed to create output directory")?;
        }

        for &input in inputs {
            let output = self.output_for(input, output)?;
            self.register_transfer(input.to_owned(), output)?;
        }

        Ok(())
    }
    fn copy(&mut self, inputs: &[&Path], output: &Path) -> Result<()> {
        if inputs.len() == 1 {
            self.copy_one(inputs[0], output)
        } else {
            self.copy_multi(inputs, output)
        }
    }
}

#[derive(Debug)]
struct Transfer {
    from: PathBuf,
    to: PathBuf,

    name: String,
    progress: AtomicU64,
}
impl Transfer {
    fn new(from: PathBuf, to: PathBuf) -> Self {
        let name = from.file_name().expect("file should always have name")
            .to_string_lossy()
            .into_owned();
        Self {
            from,
            to,

            name,
            progress: AtomicU64::new(0),
        }
    }
    async fn start(&self, bufsize: usize) -> Result<()> {
        let meta = fs::symlink_metadata(&self.from).await.context("getting symlink metadata failed")?;

        if meta.file_type().is_symlink() {
            let dest = fs::read_link(&self.from).await.context("getting symlink source")?;
            symlink(dest, &self.to).await.context("failed to copy symlink")?;

            self.progress.store(1f64.to_bits(), Ordering::Relaxed);
            return Ok(());
        }
        if meta.file_type().is_dir() {
            fs::create_dir(&self.to).await.context("failed to copy directory")?;
            self.progress.store(1f64.to_bits(), Ordering::Relaxed);
            return Ok(());
        }

        let mut from = File::open(&self.from).await.context("failed to open input file")?;
        let mut to = File::create(&self.to).await.context("failed to open/create output file")?;

        let length = from.metadata().await?.len();

        let mut buf = vec![0u8; bufsize];
        let mut transferred = 0;

        loop {
            let n = from.read(&mut buf).await?;
            if n == 0 {
                break;
            }

            to.write_all(&buf[..n]).await?;

            transferred += n as u64;
            self.progress.store(((transferred as f64) / (length as f64)).to_bits(), Ordering::Relaxed);
        }

        to.set_len(transferred).await?;

        Ok(())
    }
}
